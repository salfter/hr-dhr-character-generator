 10 D$ =  CHR$ (4): IF  PEEK (103) +  PEEK (104) * 256 <  > 16385 THEN  POKE 103,1: POKE 104,64: POKE 16384,0: PRINT D$"runhrchargen.test"
 20  PRINT D$"bloadhrchargen": PRINT D$"bloadcharset"
 30  CALL 2816: PRINT D$"pr#a$b06": PRINT D$"in#a$b0a"
 40  PRINT "Check this out:": PRINT : PRINT "(press any key to continue) ";: GET A$: HTAB 1: PRINT "                            "
 50  HCOLOR= 3:PI = 4 *  ATN (1): HPLOT 190,96: FOR I = 0 TO 2 * PI STEP PI / 60: HPLOT  TO 140 + 50 *  COS (I),96 - 50 *  SIN (I): NEXT 
 60  FOR I = 0 TO 2 * PI STEP PI / 4: HPLOT 140,96 TO 140 + 50 *  COS (I),96 - 50 *  SIN (I): NEXT 
 70  VTAB 11: HTAB 18: PRINT " Mix ": HTAB 17: PRINT " text ": HTAB 18: PRINT " and ": HTAB 15: PRINT " graphics! "
 75  PRINT D$"createtestfile,tbin":N$ = "TESTFILE": POKE 512, LEN (N$): FOR I = 1 TO  LEN (N$): POKE 512 + I, ASC ( MID$ (N$,I,1)): NEXT : CALL 2839
 80  VTAB 24: PRINT "(press any key to exit) ";: GET A$: TEXT : PRINT D$"pr#0": PRINT D$"in#0": HOME 
 90  PRINT "Enter RUN 100 to load saved file.": END 
 100 D$ =  CHR$ (4): CALL 2816: PRINT D$"pr#a$b06": PRINT D$"in#a$b0a"
 110 N$ = "TESTFILE": POKE 512, LEN (N$): FOR I = 1 TO  LEN (N$): POKE 512 + I, ASC ( MID$ (N$,I,1)): NEXT : CALL 2836
 115  VTAB 3: PRINT "This compressed file was loaded from": PRINT "disk.  It should only have taken about": PRINT "16% of the space of a normal Hi-Res": PRINT "file."
 116  VTAB 21: PRINT "Look for a file named TESTFILE and ": PRINT "see for yourself."
 120  VTAB 24: PRINT "(press any key to exit) ";: GET A$: TEXT : PRINT D$"pr#0": PRINT D$"in#0": HOME 
