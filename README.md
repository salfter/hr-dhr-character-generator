Apple II Hi-Res/Double Hi-Res Character Generator
=================================================

This package includes routines to print text to the Hi-Res and Double
Hi-Res screens.  The Hi-Res character generator (HRCHARGEN) implements a
40x24 display on a 64K II+, IIe, IIc, IIc+, or IIGS.  The Double Hi-Res
character generator implements an 80x24 display on a 128K IIe (rev. B or
later), IIc, IIc+, or IIGS.  ProDOS 8 is required; maybe you can hack it to
work under DOS 3.3, but I do all of my own programming under ProDOS and
almost never boot into DOS 3.3.  Utility routines to load/save images in
compressed and uncompressed formats are included.  The calling interface for
the two is the same, and except for HRCHARGEN supporting HTAB and VTAB, the
two are functionally equivalent.  (You need to use POKEs to do cursor
positioning with DHRCHARGEN; this isn't too different from normal 80-column
text, but the addresses are different.)  More detailed documentation is at the
top of each source file.  Assembler output has also been included, along with
ready-to-use binaries and a couple of sample programs (in BASIC) to
demonstrate how the generators work.  A font that closely duplicates the
Apple II's normal screen font is included in CHARGEN; you could replace it
with a font of your own design if you want.

Scott Alfter
27 January 2003

Addendum for Git Repo
---------------------

Here's the contents of the release file, which will give you the load
locations of the binary files in this package.  I've renamed them with a
.bin extension here.


```
 chargen.1.0.bxy Created:27-Jan-03 02:53   Mod:27-Jan-03 02:53     Recs:   12

 Name                        Type Auxtyp Archived         Fmat Size Un-Length
-----------------------------------------------------------------------------
 CHARGEN.1.0:CHARSET         BIN  $0800  27-Jan-03 02:53  lz2   65%       768
 CHARGEN.1.0:COPYING         TXT  $0000  27-Jan-03 02:53  lz2   51%     17962
 CHARGEN.1.0:DHRCHARGEN      BIN  $0B00  27-Jan-03 02:53  lz2   82%      1045
 CHARGEN.1.0:DHRCHARGEN.ASSY TXT  $0B00  27-Jan-03 02:53  lz2   39%     38202
 CHARGEN.1.0:DHRCHARGEN.SRC  TXT  $0000  27-Jan-03 02:53  lz2   55%     15550
 CHARGEN.1.0:HRCHARGEN       BIN  $0B00  27-Jan-03 02:53  lz2   86%       848
 CHARGEN.1.0:HRCHARGEN.ASSY  TXT  $0B00  27-Jan-03 02:53  lz2   39%     34084
 CHARGEN.1.0:HRCHARGEN.SRC   TXT  $0000  27-Jan-03 02:53  lz2   56%     13960
 CHARGEN.1.0:HRCHARGEN.TEST  BAS  $4001  27-Jan-03 02:53  lz2   79%      1036
 CHARGEN.1.0:Finder.Data     FND  $0000  27-Jan-03 02:53  unc  100%       276
 CHARGEN.1.0:README          TXT  $0000  27-Jan-03 02:53  lz2   71%      1233
 CHARGEN.1.0:DHRCHARGEN.TEST BAS  $4001  27-Jan-03 02:53  lz2   77%      1010
-----------------------------------------------------------------------------
```