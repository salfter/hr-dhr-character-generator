 10 D$ =  CHR$ (4): IF  PEEK (103) +  PEEK (104) * 256 <  > 16385 THEN  POKE 103,1: POKE 104,64: POKE 16384,0: PRINT D$"rundhrchargen.test"
 20  PRINT D$"bloaddhrchargen": PRINT D$"bloadcharset"
 30  CALL 2816: PRINT D$"pr#a$b06": PRINT D$"in#a$b0a"
 40  PRINT "I don't have any routines that draw DHR graphics alongside this text, but I'll": PRINT "go ahead and leave the DHR text generator connected so you can do stuff at the"
 50  PRINT "command prompt on your own.  Note that the cursor is different; it's now an": PRINT "underline.  Try doing a CATALOG.  Once the screen starts scrolling, you'll see"
 60  PRINT "some degradation in speed.  If you avoid scrolling, DHRCHARGEN runs at a": PRINT "reasonable speed.  If you have your own DHR graphics-generation routines, they"
 70  PRINT "should work alongside DHRCHARGEN.  You can also use routines provided by": PRINT "DHRCHARGEN to load and save images--either in the standard uncompressed format"
 80  PRINT "or in a new compressed format that saves a considerable amount of space.": PRINT : PRINT "To return to the normal 80-column text display, enter TEXT and then enter": PRINT "PR#3.": PRINT : PRINT "Have fun!"
