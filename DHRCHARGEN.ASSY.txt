dhrchargen 27-Jan-03

    0                  ;
    1                  ;Double Hi-Res Character Generator
    2                  ;v1.0  6 Jun 95--original release
    3                  ;v1.1  6 Aug 96--added DHR screen load
    4                  ;v1.2  4 Jun 98--added DHR screen save
    5                  ;                added compression support for load/save
    6                  ;                added handling for BEL & LF in DHRCOUT
    7                  ;v1.3 26 Jan 03--fixed HTAB/VTAB, relicensed under GNU GPL
    8                  ;
    9                  ;Copyright (C) 1995-2003 by Scott Alfter
   10                  ;scott@alfter.us
   11                  ;http://alfter.us
   12                  ;
   13                  ;This program is free software ; you can redistribute it and/or modify
   14                  ;it under the terms of the GNU General Public License as published by
   15                  ;the Free Software Foundation ; either version 2 of the License, or
   16                  ;(at your option) any later version.
   17                  ;
   18                  ;This program is distributed in the hope that it will be useful,
   19                  ;but WITHOUT ANY WARRANTY ; without even the implied warranty of
   20                  ;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   21                  ;GNU General Public License for more details.
   22                  ;
   23                  ;You should have received a copy of the GNU General Public License
   24                  ;along with this program ; if not, write to the Free Software
   25                  ;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
   26                  ;
   27                  ;Macros and pseudo-ops used in this source code are those used
   28                  ;by the MicroSPARC/MindCraft Assembler:
   29                  ;
   30                  ; USE   include macros from file
   31                  ; UEN   end of includes
   32                  ; MUL   don't expand macros in assembler output
   33                  ; HEX   include one or more hexadecimal bytes
   34                  ; ]     local label, or forward reference to a local label
   35                  ; [     backward reference to a local label
   36                  ; *     at start of line: comment
   37                  ;       in operand: current address
   38                  ;                        ;     after operand: comment
   39                  ; ^     logical AND
   40                  ; xxx/  take high byte of xxx
   41                  ; ADDR  store a 16-bit quantity in little-endian format
   42                  ; DFS   allocate space
   43                  ; ASC   store the quoted string with the high bit set
   44                  ;
   45                  ;System requirements: 128K Apple IIe (rev. B or later), IIc, IIc+, IIGS,
   46                  ;or compatible.  A 6502 is sufficient, the added instructions of the 65C02
   47                  ;and 65C816 are not used.
   48                  ;
   49                  ;Recommended use:
   50                  ;1) Relocate your BASIC program above the Hi-Res buffer with something
   51                  ;   like this:
   52                  ;   IF PEEK(103)+PEEK(104)*256<>16385 THEN POKE 103,1:POKE 104,64:
   53                  ;   PRINT CHR$(4)"RUN FOOBAR"
   54                  ;2) Load the character generator and font at their default locations:
   55                  ;   PRINT CHR$(4)"BLOAD CHARGEN":PRINT CHR$(4)"BLOAD CHARSET"
   56                  ;3) Turn on the double Hi-Res display:
   57                  ;   CALL 2816
   58                  ;4) Redirect I/O to the character generator:
   59                  ;   PRINT CHR$(4)"PR#A$B06":PRINT CHR$(4)"IN#A$B0A"
   60                  ;Normal PRINT, INPUT, and GET commands work.  HTAB and VTAB don't work ;
   61                  ;replace HTAB x with POKE 2843,x-1 and VTAB y with POKE 2844,y-1.  HOME
   62                  ;doesn't work, replace it with CALL 2816 or PRINT CHR$(12) to clear the
   63                  ;screen and move the cursor to the upper left.  Scrolling and inverse text
   64                  ;are implemented.  MouseText is not implemented.  The control characters
   65                  ;BEL, BS, LF, FF, and CR are implemented.
   66                  ;
   67                  ;Entry points:
   68                  ; dec  hex  description
   69                  ;2816  B00  switch to DHR display & clear screen
   70                  ;2819  B03  copy contents of main-memory section to auxiliary memory
   71                  ;2822  B06  character-output driver
   72                  ;2826  B0A  character-input driver
   73                  ;2830  B0E  load uncompressed DHR image
   74                  ;2833  B11  save uncompressed DHR image
   75                  ;2836  B14  load compressed DHR image
   76                  ;2839  B17  save compressed DHR image
   77                  ;
   78                  ;Note that the routines that save files won't create them if they don't
   79                  ;already exist.  Use the CREATE command to create an empty binary file,
   80                  ;like this:
   81                  ;  CREATE FOOBAR,TBIN
   82                  ;Then stuff the filename to save (or load, for that matter) into the line
   83                  ;buffer at 0x0200 with the length first and call the routine you want.
   84                  ;For example, to load a compressed-image file called FOOBAR, use this:
   85                  ;  N$="FOOBAR":POKE 512,LEN(N$):FOR I=1 TO LEN(N$):POKE 512+I,ASC(MID$(N$,
   86                  ;  I,1)):NEXT:CALL 2830
   87                  ;
   88                  ;No routines are provided here for drawing lines and other graphic items
   89                  ;on the DHR screen.  Other packages are available that can do that.  As
   90                  ;long as they don't want to use the same memory as this package, you
   91                  ;should be able to combine the two at will.  In the application for which
   92                  ;I wrote this package, the graphics were to be loaded from disk instead of
   93                  ;generated by the BASIC program, so I didn't need the ability to draw from
   94                  ;BASIC.  (The graphics I used were prepared by TimeOut Paint, which does a
   95                  ;good job of dealing with monochrome DHR graphics.)
   96                  ;
   97                  ;The compression applied here is a simple scheme that works best with
   98                  ;screens that have lots of black space ; as many as 256 bytes of black
   99                  ;area are crunched down to just two bytes ; non-black areas are stored
  100                  ;as is.  With the simple graphics I created, this yielded compression
  101                  ;anywhere from 3:1 to 11:1.  Load and save times aren't too badly affected
  102                  ;when you're working with floppy-disk storage.  The delay is noticeable
  103                  ;when you're working with hard-disk storage on an unaccelerated system,
  104                  ;but it runs well enough with a hard drive and an accelerator.
  105                  ;
  106                  ;The source code was written for the MicroSPARC Assembler ; it should be
  107                  ;possible to adapt the source code to other assemblers without too much
  108                  ;difficulty.
  109                  ;
  110                           USE MACLIB
  111                           UEN
  112                  FONTADDR EQU $0800       ;location of bitmapped screen font info
  113                  STORE80  EQU $C000       ;80STORE--softswitch for making aux mem use easier
  114                  PAGE2    EQU $C054       ;select page 1/page 2 for text & graphics
  115                  TEXT     EQU $C050       ;select graphics/text display
  116                  MIXED    EQU $C052       ;select full-screen graphics or mixed text/graphics
  117                  HIRES    EQU $C056       ;select Lo-Res/Hi-Res graphics
  118                  AN3      EQU $C05E       ;enable double Hi-Res
  119                  COL80    EQU $C00C       ;select 40/80-column display
  120                  KBD      EQU $C000       ;keyboard buffer
  121                  KBDSTRB  EQU $C010       ;keyboard strobe
  122                  TMP1     EQU $FA
  123                  TMP2     EQU $FC
  124                  TMP3     EQU $FE
  125                  SCRNPTR  EQU $3E
  126                  FONTPTR  EQU $42
  127                  INVFLG   EQU $32         ;monitor to tell if we should use normal or inverse
  128                  RNDSEED  EQU $4E         ;increment this while we wait for input
  129                  MLI      EQU $BF00       ;ProDOS 8 MLI entry point
  130                  OPEN     EQU $C8         ;selected MLI calls
  131                  READ     EQU $CA
  132                  CLOSE    EQU $CC
  133                  WRITE    EQU $CB
  134                  COUT1    EQU $FDF0       ;we need this for the bell
  135                           ORG $0B00
  136  0B00  4C 1F 0B           JMP DHRON       ;turn on the double Hi-Res display
  137  0B03  4C 57 0B           JMP MAIN2AUX    ;copy main-mem 0x2000-0x3FFF to aux-mem 0x2000-0x3FFF
  138  0B06  D8                 CLD             ;COUT replacement (hook in with PR#A$xxxx)
  139  0B07  4C 7E 0B           JMP DHRCOUT
  140  0B0A  D8                 CLD             ;RDKEY replacement (hook in with IN#A$xxxx)
  141  0B0B  4C 6D 0C           JMP DHRRDKEY
  142  0B0E  4C 78 0D           JMP LOAD        ;load uncompressed DHR image named by buffer @ 0x0200
  143  0B11  4C A3 0D           JMP SAVE        ;save uncompressed DHR image
  144  0B14  4C 7E 0E           JMP LOADC       ;load compressed DHR image
  145  0B17  4C D0 0D           JMP SAVEC       ;save compressed DHR image
  146                  CHAR     DFS 1
  147                  XPOSN    DFS 1
  148                  YPOSN    DFS 1
  149                  SAVEX    DFS 1
  150                  SAVEY    DFS 1
  151                  ;
  152                  ;Turn on DHR and clear the screen
  153                  ;
  154  0B1F  8D 52 C0  DHRON    STA MIXED       ;turn on DHR and clear screen
  155  0B22  8D 0D C0           STA COL80+1
  156  0B25  8D 57 C0           STA HIRES+1
  157  0B28  8D 5E C0           STA AN3
  158  0B2B  8D 50 C0           STA TEXT
  159  0B2E  8D 01 C0           STA STORE80+1
  160  0B31  A9 20              LDA #$2000/
  161  0B33  85 FB              STA TMP1+1
  162  0B35  A9 00              LDA #$2000
  163  0B37  85 FA              STA TMP1
  164  0B39  8D 1B 0B           STA XPOSN
  165  0B3C  8D 1C 0B           STA YPOSN
  166  0B3F  A8                 TAY
  167  0B40  8D 55 C0  ]LOOP    STA PAGE2+1
  168  0B43  91 FA              STA (TMP1),Y
  169  0B45  8D 54 C0           STA PAGE2
  170  0B48  91 FA              STA (TMP1),Y
  171  0B4A  E6 FA              INC TMP1
  172  0B4C  D0 F2              BNE [LOOP
  173  0B4E  E6 FB              INC TMP1+1
  174  0B50  A6 FB              LDX TMP1+1
  175  0B52  E0 40              CPX #$4000/
  176  0B54  D0 EA              BNE [LOOP
  177  0B56  60                 RTS
  178                  ;
  179                  ;Copy contents of the main-memory section of the DHR screen
  180                  ;to the auxiliary-memory section
  181                  ;
  182  0B57  8D 01 C0  MAIN2AUX STA STORE80+1
  183  0B5A  A9 00              LDA #$2000
  184  0B5C  85 FA              STA TMP1
  185  0B5E  A9 20              LDA #$2000/
  186  0B60  85 FB              STA TMP1+1
  187  0B62  A0 00              LDY #0
  188  0B64  8D 54 C0  ]LOOP    STA PAGE2
  189  0B67  B1 FA              LDA (TMP1),Y
  190  0B69  8D 55 C0           STA PAGE2+1
  191  0B6C  91 FA              STA (TMP1),Y
  192  0B6E  E6 FA              INC TMP1
  193  0B70  D0 F2              BNE [LOOP
  194  0B72  E6 FB              INC TMP1+1
  195  0B74  A5 FB              LDA TMP1+1
  196  0B76  C9 40              CMP #$4000/
  197  0B78  D0 EA              BNE [LOOP
  198  0B7A  8D 54 C0           STA PAGE2
  199  0B7D  60                 RTS
  200                  ;
  201                  ;DHR character-output routine
  202                  ;
  203  0B7E  8D 1A 0B  DHRCOUT  STA CHAR
  204  0B81  8E 1D 0B           STX SAVEX
  205  0B84  8C 1E 0B           STY SAVEY
  206  0B87  08                 PHP
  207  0B88  29 7F              AND #$7F        ;strip high bit
  208  0B8A  C9 20              CMP #$20        ;printable character?
  209  0B8C  B0 5D              BCS ]GO
  210  0B8E  C9 0D              CMP #$0D        ;CR?
  211  0B90  F0 18              BEQ ]RTN
  212  0B92  C9 08              CMP #$08        ;BS?
  213  0B94  F0 3B              BEQ ]BS
  214  0B96  C9 0C              CMP #$0C        ;FF?
  215  0B98  F0 4B              BEQ ]HOME
  216  0B9A  C9 07              CMP #$07        ;BEL?
  217  0B9C  F0 2B              BEQ ]BELL
  218  0B9E  C9 0A              CMP #$0A        ;LF?
  219  0BA0  F0 0D              BEQ ]LF
  220  0BA2  28                 PLP             ;not valid, so exit
  221  0BA3  AE 1D 0B           LDX SAVEX
  222  0BA6  AC 1E 0B           LDY SAVEY
  223  0BA9  60                 RTS
  224  0BAA  A9 00     ]RTN     LDA #0          ;CR: move cursor to left edge
  225  0BAC  8D 1B 0B           STA XPOSN
  226  0BAF  EE 1C 0B  ]LF      INC YPOSN       ;CR/LF: move cursor down one line
  227  0BB2  AD 1C 0B           LDA YPOSN
  228  0BB5  C9 18              CMP #24         ;need to scroll?
  229  0BB7  D0 08              BNE ]EXIT
  230  0BB9  A9 17              LDA #23         ;put cursor at bottom line and scroll
  231  0BBB  8D 1C 0B           STA YPOSN
  232  0BBE  20 19 0D           JSR SCROLL
  233  0BC1  AE 1D 0B  ]EXIT    LDX SAVEX
  234  0BC4  AC 1E 0B           LDY SAVEY
  235  0BC7  28                 PLP
  236  0BC8  60                 RTS
  237  0BC9  09 80     ]BELL    ORA #$80        ;BEL: pass to COUT1 in ROM for sys beep
  238  0BCB  20 F0 FD           JSR COUT1
  239  0BCE  4C C1 0B           JMP [EXIT
  240  0BD1  CE 1B 0B  ]BS      DEC XPOSN       ;BS: move cursor back a space, possibly to
  241  0BD4  10 EB              BPL [EXIT       ;previous line
  242  0BD6  A9 4F              LDA #79
  243  0BD8  8D 1B 0B           STA XPOSN
  244  0BDB  CE 1C 0B           DEC YPOSN
  245  0BDE  10 E1              BPL ]EXIT
  246  0BE0  A9 00              LDA #0
  247  0BE2  8D 1C 0B           STA YPOSN
  248  0BE5  20 1F 0B  ]HOME    JSR DHRON       ;FF: clear screen and move cursor to top
  249  0BE8  4C C1 0B           JMP [EXIT
  250  0BEB  E9 20     ]GO      SBC #$20        ;get index into font
  251  0BED  8D 1A 0B           STA CHAR
  252  0BF0  AD 1B 0B           LDA XPOSN       ;get display coordinates
  253  0BF3  85 FC              STA TMP2
  254  0BF5  AD 1C 0B           LDA YPOSN
  255  0BF8  85 FD              STA TMP2+1
  256  0BFA  20 D7 0C           JSR BASECALC
  257  0BFD  AD 1A 0B           LDA CHAR
  258  0C00  85 42              STA FONTPTR
  259  0C02  A9 00              LDA #0
  260  0C04  85 43              STA FONTPTR+1
  261  0C06  A2 03              LDX #3          ;multiply by 8
  262  0C08  06 42     ]LOOP    ASL FONTPTR
  263  0C0A  26 43              ROL FONTPTR+1
  264  0C0C  CA                 DEX
  265  0C0D  D0 F9              BNE [LOOP
  266  0C0F  18                 CLC
  267  0C10  A5 42              LDA FONTPTR     ;find address of character to draw
  268  0C12  69 00              ADC #FONTADDR
  269  0C14  85 42              STA FONTPTR
  270  0C16  A5 43              LDA FONTPTR+1
  271  0C18  69 08              ADC #FONTADDR/
  272  0C1A  85 43              STA FONTPTR+1
  273  0C1C  AD 1B 0B           LDA XPOSN       ;set write to aux or main mem as appropriate
  274  0C1F  29 01              AND #1
  275  0C21  49 01              EOR #1
  276  0C23  AA                 TAX
  277  0C24  9D 54 C0           STA PAGE2,X
  278  0C27  8D 01 C0           STA STORE80+1
  279  0C2A  A0 00              LDY #0
  280  0C2C  A2 00              LDX #0
  281  0C2E  B1 42     ]LOOP    LDA (FONTPTR),Y  ;get part of character from font
  282  0C30  24 32              BIT INVFLG      ;should it be drawn in inverse?
  283  0C32  30 02              BMI ]NORMAL
  284  0C34  49 7F              EOR #$7F        ;invert it
  285  0C36  81 3E     ]NORMAL  STA (SCRNPTR,X)  ;write it to the screen
  286  0C38  18                 CLC             ;move to the next line
  287  0C39  A5 3F              LDA SCRNPTR+1
  288  0C3B  69 04              ADC #4
  289  0C3D  85 3F              STA SCRNPTR+1
  290  0C3F  C8                 INY
  291  0C40  C0 08              CPY #8          ;we have 8 lines to draw
  292  0C42  D0 EA              BNE [LOOP
  293  0C44  EE 1B 0B           INC XPOSN       ;move cursor to next space
  294  0C47  AD 1B 0B           LDA XPOSN
  295  0C4A  C9 50              CMP #80         ;move to next line if needed
  296  0C4C  D0 17              BNE ]QUIT
  297  0C4E  A9 00              LDA #0
  298  0C50  8D 1B 0B           STA XPOSN
  299  0C53  EE 1C 0B           INC YPOSN
  300  0C56  AD 1C 0B           LDA YPOSN
  301  0C59  C9 18              CMP #24         ;scroll if needed
  302  0C5B  D0 08              BNE ]QUIT
  303  0C5D  A9 17              LDA #23
  304  0C5F  8D 1C 0B           STA YPOSN
  305  0C62  20 19 0D           JSR SCROLL
  306  0C65  28        ]QUIT    PLP
  307  0C66  AE 1D 0B           LDX SAVEX
  308  0C69  AC 1E 0B           LDY SAVEY
  309  0C6C  60                 RTS
  310                  ;
  311                  ;Put up a cursor, get keyboard input, and erase the cursor
  312                  ;
  313  0C6D  08        DHRRDKEY PHP
  314  0C6E  8E 1D 0B           STX SAVEX
  315  0C71  8C 1E 0B           STY SAVEY
  316  0C74  AD 1B 0B           LDA XPOSN       ;find memory address for cursor position
  317  0C77  85 FC              STA TMP2
  318  0C79  AD 1C 0B           LDA YPOSN
  319  0C7C  85 FD              STA TMP2+1
  320  0C7E  20 D7 0C           JSR BASECALC
  321  0C81  AD 1B 0B           LDA XPOSN       ;place it in main or auxiliary memory
  322  0C84  29 01              AND #1
  323  0C86  49 01              EOR #1
  324  0C88  AA                 TAX
  325  0C89  9D 54 C0           STA PAGE2,X
  326  0C8C  18                 CLC             ;invert the bottom two rows...this is the cursor
  327  0C8D  A5 3F              LDA SCRNPTR+1
  328  0C8F  69 18              ADC #24
  329  0C91  85 3F              STA SCRNPTR+1
  330  0C93  A2 00              LDX #0
  331  0C95  A1 3E              LDA (SCRNPTR,X)
  332  0C97  49 7F              EOR #$7F
  333  0C99  81 3E              STA (SCRNPTR,X)
  334  0C9B  A5 3F              LDA SCRNPTR+1
  335  0C9D  69 04              ADC #4
  336  0C9F  85 3F              STA SCRNPTR+1
  337  0CA1  A1 3E              LDA (SCRNPTR,X)
  338  0CA3  49 7F              EOR #$7F
  339  0CA5  81 3E              STA (SCRNPTR,X)
  340  0CA7  E6 4E     ]LOOP    INC RNDSEED     ;bump the random seed
  341  0CA9  D0 02              BNE ]CHECK
  342  0CAB  E6 4F              INC RNDSEED+1
  343  0CAD  AD 00 C0  ]CHECK   LDA KBD         ;check keyboard
  344  0CB0  10 F5              BPL [LOOP       ;pressed?
  345  0CB2  48                 PHA             ;yes...save it while we erase the cursor
  346  0CB3  A9 00              LDA #0
  347  0CB5  8D 10 C0           STA KBDSTRB     ;clear keyboard
  348  0CB8  A1 3E              LDA (SCRNPTR,X)  ;erase cursor
  349  0CBA  49 7F              EOR #$7F
  350  0CBC  81 3E              STA (SCRNPTR,X)
  351  0CBE  38                 SEC
  352  0CBF  A5 3F              LDA SCRNPTR+1
  353  0CC1  E9 04              SBC #4
  354  0CC3  85 3F              STA SCRNPTR+1
  355  0CC5  A1 3E              LDA (SCRNPTR,X)
  356  0CC7  49 7F              EOR #$7F
  357  0CC9  81 3E              STA (SCRNPTR,X)
  358  0CCB  68                 PLA             ;get keypress back
  359  0CCC  28                 PLP
  360  0CCD  AE 1D 0B           LDX SAVEX
  361  0CD0  AC 1E 0B           LDY SAVEY
  362  0CD3  8D 54 C0           STA PAGE2
  363  0CD6  60                 RTS
  364                  ;
  365                  ;Calculate a screen address from X- and Y-coordinates
  366                  ;input: X-coordinate in TMP2, Y-coordinate in TMP2+1
  367                  ;
  368  0CD7  A9 00     BASECALC LDA #$2000      ;screen starts at 0x2000
  369  0CD9  85 3E              STA SCRNPTR
  370  0CDB  A9 20              LDA #$2000/
  371  0CDD  85 3F              STA SCRNPTR+1
  372  0CDF  A5 FD              LDA TMP2+1      ;INT(Y/8)*8
  373  0CE1  29 F8              AND #$F8
  374  0CE3  48                 PHA             ;INT(Y/8)*40
  375  0CE4  0A                 ASL
  376  0CE5  0A                 ASL
  377  0CE6  18                 CLC
  378  0CE7  65 3E              ADC SCRNPTR
  379  0CE9  85 3E              STA SCRNPTR
  380  0CEB  68                 PLA
  381  0CEC  65 3E              ADC SCRNPTR
  382  0CEE  85 3E              STA SCRNPTR
  383  0CF0  A5 FD              LDA TMP2+1      ;Y-INT(Y/8)*8
  384  0CF2  29 07              AND #7
  385  0CF4  85 FA              STA TMP1
  386  0CF6  A9 00              LDA #0
  387  0CF8  85 FB              STA TMP1+1
  388  0CFA  A2 07              LDX #7
  389  0CFC  06 FA     ]LOOP    ASL TMP1
  390  0CFE  26 FB              ROL TMP1+1
  391  0D00  CA                 DEX
  392  0D01  D0 F9              BNE [LOOP
  393  0D03  18                 CLC             ;INT(Y/8)*40+(Y-INT(Y/8)*8)*128
  394  0D04  A5 3E              LDA SCRNPTR
  395  0D06  65 FA              ADC TMP1
  396  0D08  85 3E              STA SCRNPTR
  397  0D0A  A5 3F              LDA SCRNPTR+1
  398  0D0C  65 FB              ADC TMP1+1
  399  0D0E  85 3F              STA SCRNPTR+1
  400  0D10  A5 FC              LDA TMP2        ;SCRNPTR=8192+INT(Y/8)*40+(Y-INT(Y/8)*8)*128+INT(X/2)
  401  0D12  4A                 LSR             ;(even X in aux mem, odd X in main mem)
  402  0D13  18                 CLC
  403  0D14  65 3E              ADC SCRNPTR
  404  0D16  85 3E              STA SCRNPTR
  405  0D18  60                 RTS
  406                  ;
  407                  ;scroll the screen up 1 line
  408                  ;
  409  0D19  A9 00     SCROLL   LDA #0          ;start at top: move line 1 to line 0, etc.
  410  0D1B  85 FC              STA TMP2
  411  0D1D  85 FD              STA TMP2+1
  412  0D1F  20 D7 0C  ]LOOP1   JSR BASECALC    ;get destination address
  413  0D22  A5 3E              LDA SCRNPTR     ;save it
  414  0D24  85 FE              STA TMP3
  415  0D26  A5 3F              LDA SCRNPTR+1
  416  0D28  85 FF              STA TMP3+1
  417  0D2A  E6 FD              INC TMP2+1      ;get source address
  418  0D2C  20 D7 0C           JSR BASECALC
  419  0D2F  A2 08              LDX #8          ;eight rows of pixels in each line
  420  0D31  A0 27     ]LOOP2   LDY #39         ;40 bytes in each of main & aux mem for each line
  421  0D33  8D 55 C0  ]LOOP3   STA PAGE2+1     ;move a byte in aux mem
  422  0D36  B1 3E              LDA (SCRNPTR),Y
  423  0D38  91 FE              STA (TMP3),Y
  424  0D3A  8D 54 C0           STA PAGE2       ;move a byte in main mem
  425  0D3D  B1 3E              LDA (SCRNPTR),Y
  426  0D3F  91 FE              STA (TMP3),Y
  427  0D41  88                 DEY             ;do the rest of the row of pixels
  428  0D42  10 EF              BPL [LOOP3
  429  0D44  18                 CLC             ;move down to the next row of pixels
  430  0D45  A5 3F              LDA SCRNPTR+1
  431  0D47  69 04              ADC #4
  432  0D49  85 3F              STA SCRNPTR+1
  433  0D4B  A5 FF              LDA TMP3+1
  434  0D4D  69 04              ADC #4
  435  0D4F  85 FF              STA TMP3+1
  436  0D51  CA                 DEX
  437  0D52  D0 DD              BNE [LOOP2      ;repeat until we've done all eight rows
  438  0D54  A5 FD              LDA TMP2+1
  439  0D56  C9 17              CMP #23         ;did we just move the bottom line up?
  440  0D58  90 C5              BCC [LOOP1
  441  0D5A  A2 08              LDX #8          ;clear the bottom line
  442  0D5C  38        ]LOOP1   SEC
  443  0D5D  A5 3F              LDA SCRNPTR+1
  444  0D5F  E9 04              SBC #4
  445  0D61  85 3F              STA SCRNPTR+1
  446  0D63  A9 00              LDA #0
  447  0D65  A0 27              LDY #39
  448  0D67  8D 55 C0  ]LOOP2   STA PAGE2+1
  449  0D6A  91 3E              STA (SCRNPTR),Y
  450  0D6C  8D 54 C0           STA PAGE2
  451  0D6F  91 3E              STA (SCRNPTR),Y
  452  0D71  88                 DEY
  453  0D72  10 F3              BPL [LOOP2
  454  0D74  CA                 DEX
  455  0D75  D0 E5              BNE [LOOP1
  456  0D77  60                 RTS
  457                  ;
  458                  ;load DHR image from disk into memory (pathname at 0x0200)
  459                  ;
  460  0D78  20 00 BF  LOAD     JSR MLI         ;open file
  461  0D7B  C8                 DFC OPEN
  462                           ADDR OPENB
  462  0D7C  EB 0E              DFC OPENB,OPENB/256
  462                           EMC
  463  0D7E  AD F0 0E           LDA OPENB+5     ;get reference number
  464  0D81  8D F2 0E           STA READB+1     ;save it
  465  0D84  8D FA 0E           STA CLOSEB+1
  466  0D87  8D 01 C0           STA STORE80+1
  467  0D8A  8D 55 C0           STA PAGE2+1
  468  0D8D  20 00 BF           JSR MLI         ;get the aux-mem portion of the file
  469  0D90  CA                 DFC READ
  470                           ADDR READB
  470  0D91  F1 0E              DFC READB,READB/256
  470                           EMC
  471                  ; JSR MAIN2AUX           ;move it into place
  472  0D93  8D 54 C0           STA PAGE2
  473  0D96  20 00 BF           JSR MLI         ;get the main-mem portion of the file
  474  0D99  CA                 DFC READ
  475                           ADDR READB
  475  0D9A  F1 0E              DFC READB,READB/256
  475                           EMC
  476  0D9C  20 00 BF           JSR MLI         ;close the file
  477  0D9F  CC                 DFC CLOSE
  478                           ADDR CLOSEB
  478  0DA0  F9 0E              DFC CLOSEB,CLOSEB/256
  478                           EMC
  479  0DA2  60                 RTS             ;done
  480                  ;
  481                  ;save uncompressed DHR image to disk
  482                  ;
  483  0DA3  20 00 BF  SAVE     JSR MLI         ;open file (must already exist)
  484  0DA6  C8                 DFC OPEN
  485                           ADDR OPENB
  485  0DA7  EB 0E              DFC OPENB,OPENB/256
  485                           EMC
  486  0DA9  D0 24              BNE ]ERROR
  487  0DAB  AD F0 0E           LDA OPENB+5     ;get reference number
  488  0DAE  8D FC 0E           STA WRITEB+1
  489  0DB1  8D FA 0E           STA CLOSEB+1
  490  0DB4  8D 01 C0           STA STORE80+1   ;save aux-mem section first
  491  0DB7  8D 55 C0           STA PAGE2+1
  492  0DBA  20 00 BF           JSR MLI
  493  0DBD  CB                 DFC WRITE
  494                           ADDR WRITEB
  494  0DBE  FB 0E              DFC WRITEB,WRITEB/256
  494                           EMC
  495  0DC0  8D 54 C0           STA PAGE2       ;then main-mem section
  496  0DC3  20 00 BF           JSR MLI
  497  0DC6  CB                 DFC WRITE
  498                           ADDR WRITEB
  498  0DC7  FB 0E              DFC WRITEB,WRITEB/256
  498                           EMC
  499  0DC9  20 00 BF           JSR MLI         ;close
  500  0DCC  CC                 DFC CLOSE
  501                           ADDR CLOSEB
  501  0DCD  F9 0E              DFC CLOSEB,CLOSEB/256
  501                           EMC
  502  0DCF  60        ]ERROR   RTS             ;exit
  503                  ;
  504                  ;compress DHR screen contents into a file (name @ 0x0200)
  505                  ;
  506  0DD0  8D 01 C0  SAVEC    STA STORE80+1   ;make sure screen flags are set
  507  0DD3  A9 78              LDA #$2078      ;zero out screen holes to maximize compresion
  508  0DD5  85 FA              STA TMP1
  509  0DD7  A9 20              LDA #$2078/
  510  0DD9  85 FB              STA TMP1+1
  511  0DDB  A9 00     ]ZERO    LDA #0
  512  0DDD  A0 07              LDY #7
  513  0DDF  8D 55 C0  ]LOOP    STA PAGE2+1
  514  0DE2  91 FA              STA (TMP1),Y
  515  0DE4  8D 54 C0           STA PAGE2
  516  0DE7  91 FA              STA (TMP1),Y
  517  0DE9  88                 DEY
  518  0DEA  10 F3              BPL [LOOP
  519  0DEC  18                 CLC
  520  0DED  A5 FA              LDA TMP1
  521  0DEF  69 80              ADC #$80
  522  0DF1  85 FA              STA TMP1
  523  0DF3  A5 FB              LDA TMP1+1
  524  0DF5  69 00              ADC #$80/
  525  0DF7  85 FB              STA TMP1+1
  526  0DF9  C9 40              CMP #$4000/
  527  0DFB  D0 DE              BNE [ZERO
  528  0DFD  A9 01              LDA #1
  529  0DFF  85 FC              STA TMP2
  530  0E01  8D 54 C0           STA PAGE2       ;make sure compressor doesn't go off-screen
  531  0E04  8D FF 3F           STA $3FFF
  532  0E07  8D 55 C0           STA PAGE2+1
  533  0E0A  8D FF 3F           STA $3FFF
  534  0E0D  20 00 BF           JSR MLI         ;open file (must already exist)
  535  0E10  C8                 DFC OPEN
  536                           ADDR OPENB
  536  0E11  EB 0E              DFC OPENB,OPENB/256
  536                           EMC
  537  0E13  D0 68              BNE ]ERROR
  538  0E15  AD F0 0E           LDA OPENB+5     ;get reference number
  539  0E18  8D 04 0F           STA WRITECB+1   ;save it
  540  0E1B  8D FA 0E           STA CLOSEB+1
  541  0E1E  A9 00     ]REPEAT  LDA #$2000      ;do aux-mem, then main-mem
  542  0E20  85 FA              STA TMP1
  543  0E22  A9 20              LDA #$2000/
  544  0E24  85 FB              STA TMP1+1
  545  0E26  A4 FC              LDY TMP2
  546  0E28  99 54 C0           STA PAGE2,Y
  547  0E2B  A0 00     ]LOOP    LDY #0          ;get screen byte
  548  0E2D  B1 FA              LDA (TMP1),Y
  549  0E2F  F0 12              BEQ ]CRUNCH     ;if zero, count 'em
  550  0E31  8D 13 0F           STA CBUF        ;put it in the output buffer
  551  0E34  C8                 INY             ;write one byte
  552  0E35  8C 07 0F           STY WRITECB+4
  553  0E38  8C 14 0F           STY CBUF+1
  554  0E3B  20 00 BF           JSR MLI
  555  0E3E  CB                 DFC WRITE
  556                           ADDR WRITECB
  556  0E3F  03 0F              DFC WRITECB,WRITECB/256
  556                           EMC
  557  0E41  F0 18              BEQ ]NEXT
  558  0E43  C8        ]CRUNCH  INY             ;count consecutive zeros
  559  0E44  F0 04              BEQ ]CRUNCHED   ;up to 256
  560  0E46  D1 FA              CMP (TMP1),Y
  561  0E48  F0 F9              BEQ [CRUNCH
  562  0E4A  8D 13 0F  ]CRUNCHED STA CBUF       ;put the results in the output buffer
  563  0E4D  8C 14 0F           STY CBUF+1
  564  0E50  A9 02              LDA #2
  565  0E52  8D 07 0F           STA WRITECB+4
  566  0E55  20 00 BF           JSR MLI         ;write it
  567  0E58  CB                 DFC WRITE
  568                           ADDR WRITECB
  568  0E59  03 0F              DFC WRITECB,WRITECB/256
  568                           EMC
  569  0E5B  18        ]NEXT    CLC
  570  0E5C  A5 FA              LDA TMP1        ;increment counter
  571  0E5E  6D 14 0F           ADC CBUF+1
  572  0E61  85 FA              STA TMP1
  573  0E63  AD 14 0F           LDA CBUF+1
  574  0E66  D0 01              BNE ]NOT256
  575  0E68  38                 SEC
  576  0E69  A5 FB     ]NOT256  LDA TMP1+1
  577  0E6B  69 00              ADC #0
  578  0E6D  85 FB              STA TMP1+1
  579  0E6F  C9 40              CMP #$4000/     ;end of screen buffer?
  580  0E71  D0 B8              BNE [LOOP
  581  0E73  C6 FC              DEC TMP2        ;switch to main mem, or quit if done already
  582  0E75  10 A7              BPL [REPEAT
  583  0E77  20 00 BF           JSR MLI         ;close file and quit
  584  0E7A  CC                 DFC CLOSE
  585                           ADDR CLOSEB
  585  0E7B  F9 0E              DFC CLOSEB,CLOSEB/256
  585                           EMC
  586  0E7D  60        ]ERROR   RTS
  587                  ;
  588                  ;load compressed picture
  589                  ;
  590  0E7E  8D 01 C0  LOADC    STA STORE80+1   ;make sure screen flags are set
  591  0E81  A9 01              LDA #1          ;start with aux mem
  592  0E83  85 FC              STA TMP2
  593  0E85  20 00 BF           JSR MLI         ;open file
  594  0E88  C8                 DFC OPEN
  595                           ADDR OPENB
  595  0E89  EB 0E              DFC OPENB,OPENB/256
  595                           EMC
  596  0E8B  D0 5D              BNE ]ERROR
  597  0E8D  AD F0 0E           LDA OPENB+5     ;get reference number
  598  0E90  8D 0C 0F           STA READCB+1    ;save it
  599  0E93  8D FA 0E           STA CLOSEB+1
  600  0E96  A9 00     ]REPEAT  LDA #$2000      ;start at beginning of scrn area
  601  0E98  85 FA              STA TMP1
  602  0E9A  A9 20              LDA #$2000/
  603  0E9C  85 FB              STA TMP1+1
  604  0E9E  A4 FC              LDY TMP2        ;select proper section to work with
  605  0EA0  99 54 C0           STA PAGE2,Y
  606  0EA3  20 00 BF  ]LOOP    JSR MLI         ;get a byte
  607  0EA6  CA                 DFC READ
  608                           ADDR READCB
  608  0EA7  0B 0F              DFC READCB,READCB/256
  608                           EMC
  609  0EA9  AD 13 0F           LDA CBUF
  610  0EAC  F0 0A              BEQ ]EXPAND     ;if zero, expand it out
  611  0EAE  A0 00              LDY #0
  612  0EB0  91 FA              STA (TMP1),Y    ;write it to memory
  613  0EB2  C8                 INY
  614  0EB3  8C 13 0F           STY CBUF
  615  0EB6  D0 10              BNE ]NEXT
  616  0EB8  20 00 BF  ]EXPAND  JSR MLI         ;get number of zeros
  617  0EBB  CA                 DFC READ
  618                           ADDR READCB
  618  0EBC  0B 0F              DFC READCB,READCB/256
  618                           EMC
  619  0EBE  AC 13 0F           LDY CBUF
  620  0EC1  A9 00              LDA #0
  621  0EC3  88        ]EXPLP   DEY             ;write out the zeros
  622  0EC4  91 FA              STA (TMP1),Y
  623  0EC6  D0 FB              BNE [EXPLP
  624  0EC8  18        ]NEXT    CLC             ;increment counter
  625  0EC9  A5 FA              LDA TMP1
  626  0ECB  6D 13 0F           ADC CBUF
  627  0ECE  85 FA              STA TMP1
  628  0ED0  AD 13 0F           LDA CBUF
  629  0ED3  D0 01              BNE ]NOT256
  630  0ED5  38                 SEC
  631  0ED6  A5 FB     ]NOT256  LDA TMP1+1
  632  0ED8  69 00              ADC #0
  633  0EDA  85 FB              STA TMP1+1
  634  0EDC  C9 40              CMP #$4000/     ;end of screen?
  635  0EDE  D0 C3              BNE [LOOP
  636  0EE0  C6 FC              DEC TMP2        ;switch to main mem, or quit if done already
  637  0EE2  10 B2              BPL [REPEAT
  638  0EE4  20 00 BF           JSR MLI         ;close file
  639  0EE7  CC                 DFC CLOSE
  640                           ADDR CLOSEB
  640  0EE8  F9 0E              DFC CLOSEB,CLOSEB/256
  640                           EMC
  641  0EEA  60        ]ERROR   RTS             ;exit
  642                  ;
  643                  ;parameter blocks for MLI calls
  644                  ;
  645  0EEB  03        OPENB    DFC 3
  646                           ADDR $200       ;pathname buffer
  646  0EEC  00 02              DFC $200 ,$200 /256
  646                           EMC
  647                           ADDR $1C00      ;I/O buffer (just below page 1)
  647  0EEE  00 1C              DFC $1C00 ,$1C00 /256
  647                           EMC
  648                           DFS 1           ;file reference number
  649  0EF1  04        READB    DFC 4
  650                           DFS 1           ;file reference number
  651                           ADDR $2000      ;load into 0x2000
  651  0EF3  00 20              DFC $2000 ,$2000 /256
  651                           EMC
  652                           ADDR $2000      ;load 0x2000 bytes
  652  0EF5  00 20              DFC $2000 ,$2000 /256
  652                           EMC
  653                           DFS 2
  654  0EF9  01        CLOSEB   DFC 1
  655                           DFS 1           ;file reference number
  656  0EFB  04        WRITEB   DFC 4
  657                           DFS 1           ;file reference number
  658                           ADDR $2000      ;save from 0x2000
  658  0EFD  00 20              DFC $2000 ,$2000 /256
  658                           EMC
  659                           ADDR $2000      ;save 0x2000 bytes
  659  0EFF  00 20              DFC $2000 ,$2000 /256
  659                           EMC
  660                           DFS 2           ;bytes written
  661  0F03  04        WRITECB  DFC 4
  662                           DFS 1           ;file reference number
  663                           ADDR CBUF       ;output buffer
  663  0F05  13 0F              DFC CBUF ,CBUF /256
  663                           EMC
  664                           ADDR 0          ;bytes to write
  664  0F07  00 00              DFC 0 ,0 /256
  664                           EMC
  665                           DFS 2           ;bytes written
  666  0F0B  04        READCB   DFC 4
  667                           DFS 1           ;file reference number
  668                           ADDR CBUF       ;input buffer
  668  0F0D  13 0F              DFC CBUF ,CBUF /256
  668                           EMC
  669                           ADDR 1          ;bytes to read
  669  0F0F  01 00              DFC 1 ,1 /256
  669                           EMC
  670                           DFS 2           ;bytes read
  671                  CBUF     DFS 2           ;buffer for read/write of compressed files
